module Control.Monad.IO.StdErr where

import System.IO

putErrLn :: String -> IO ()
putErrLn = hPutStrLn stderr
