-- |Handles loading of config files

{-# LANGUAGE DeriveGeneric #-}

module Network.Erina.Config (getConf, Config(..)) where

import Control.Exception
import GHC.Generics
import Data.Yaml
import Data.Text

-- |Data type for storing main configuration data
data Config = Config
              { storeDir        :: String
              , dbName          :: String
              , dbAddr          :: String
              , dbPort          :: Int
              , dbLogFile       :: String
              , discordWebHook  :: Maybe String
              , discordPostTags :: [[Text]]
              , discordDelay    :: Maybe Int
              , maxFileLen      :: Int
              , tags            :: [String]
              , updateDelay     :: Int
              } deriving ( Show, Generic )
instance FromJSON Config

defaultConfLoc :: String
defaultConfLoc = "./default.yaml"

userConfLoc :: String
userConfLoc = "./conf.yaml"

-- |Attempt to load the user config file, and if that fails, attempt to load the default config.
getConf :: IO Config
getConf
  = do
    userConf <- try (loadConfigFile userConfLoc) :: IO (Either ParseException (Maybe Config))
    case userConf of
      Left _          -> defaultConf
      Right Nothing   -> defaultConf
      Right (Just c)  -> return c
    where
      defaultConf = do
                      defaultConf' <- loadConfigFile defaultConfLoc
                      case defaultConf' of
                        Nothing -> error "Could not load user config or default fallback config"
                        Just c  -> return c
    

loadConfigFile :: String -> IO (Maybe Config)
loadConfigFile confFile
  = decodeFile confFile :: IO (Maybe Config)
