-- |Functions and data types for communication with database backend

{-# LANGUAGE EmptyDataDecls             #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}

module Network.Erina.DataStore  ( checkDupeLink
                                , checkDupeHash
                                , assignFile
                                , insertTarget
                                , getLatestSourceLocal
                                , getNullDownloads
                                , getTaggedPosts
                                , getEntryById
                                , Entry (..)
                                , EntryId) where

import Network.Erina.Types.Target
import Network.Erina.Config

import Control.Monad
import Control.Monad.Trans.Control
import Control.Monad.Trans.Resource
import Control.Monad.Logger
import Control.Monad.Reader
import Control.Monad.IO.Class  (liftIO)
import Data.Time.Clock
import Data.Time.Calendar
import Data.Text
import qualified Data.ByteString.Char8 as BS
import Database.Persist
import Database.Persist.Postgresql
import Database.Persist.TH
import Network (PortID (PortNumber), PortNumber)


share [mkPersist sqlSettings, mkMigrate "migrateAll"] [persistLowerCase|
Entry
  source DlSource
  srcId Text
  md5 Text Maybe
  srcUrl Text
  thumbLoc Text Maybe
  imgLoc Text Maybe
  character Text Maybe
  artist Text Maybe
  uploadTime UTCTime
  miscText Text
  deriving Show
TagMatch
  tag Text
  taggedEntry EntryId
  deriving Show
|]

minTime :: UTCTime
minTime = UTCTime (ModifiedJulianDay 0) (secondsToDiffTime 0)

connections :: Int
connections = 1


targetToEntry :: DlTarget -> ([Text], Entry)
targetToEntry tg
  = (tags, entry)
    where
      tags = dlTags tg
      entry = Entry (dlSource tg) (dlId tg) (fmap pack $ md5 tg) (dlUrl tg) (dlThumbUrl tg) Nothing (dlCharacter tg) (dlArtist tg) (targetUploadTime tg) (targetLinkedText tg)

entryToPartialTarget :: Entry -> DlTarget
entryToPartialTarget e
  = DlTarget (entrySource e) (entrySrcUrl e) (entryThumbLoc e) (entrySrcId e) (entryCharacter e) (entryArtist e) [] (fmap unpack $ entryMd5 e) (entryUploadTime e) (entryMiscText e)


runLogger conf = runNoLoggingT


-- |Assigns a local file location to an existing target in the database
assignFile :: DlTarget -> Config -> Text -> IO ()
assignFile tg conf fileLoc
  = void $ runLogger conf $ withPostgresqlPool (getConnStr conf) connections $ \pool -> liftIO $ do
      flip runSqlPersistMPool pool . asSqlBackendReader $ do
        runMigration migrateAll
        let src = dlSource tg
        let url = dlUrl tg
        let dlid = dlId tg
        updateWhere [EntrySource ==. src, EntrySrcUrl ==. url, EntrySrcId ==. dlid] [EntryImgLoc =. Just fileLoc]

-- |Inserts a new target into the database
insertTarget :: DlTarget -> Config -> IO ()
insertTarget target conf
  = void $ runLogger conf $ withPostgresqlPool (getConnStr conf) connections $ \pool -> liftIO $ do
      flip runSqlPersistMPool pool . asSqlBackendReader $ do
        runMigration migrateAll
        let (tags, entry) = targetToEntry target
        eid <- insert entry
        mapM_ (\t -> insert $ TagMatch t eid) tags

-- |Returns the update time of the latest post for a given content source
getLatestSourceLocal :: DlSource -> Config -> IO UTCTime
getLatestSourceLocal src conf
  = runLogger conf $ withPostgresqlPool (getConnStr conf) connections $ \pool -> liftIO $ do
      flip runSqlPersistMPool pool . asSqlBackendReader $ do
        runMigration migrateAll
        earlyDate <- selectList [EntrySource ==. src] [Desc EntryUploadTime, LimitTo 1]
        return $ if Prelude.null earlyDate then minTime else entryUploadTime $ entityVal (earlyDate !! 0) 

-- |Returns a list of download targets with no associated local file location
getNullDownloads :: Config -> IO [DlTarget]
getNullDownloads conf
  = runLogger conf $ withPostgresqlPool (getConnStr conf) connections $ \pool -> liftIO $ do
      flip runSqlPersistMPool pool . asSqlBackendReader $ do
        runMigration migrateAll
        liftM (Prelude.map $ entryToPartialTarget . entityVal) $ selectList [EntryImgLoc ==. Nothing] [Asc EntryUploadTime]

-- |Returns True iff there is already a database entry for an image at the same URL
checkDupeLink :: Config -> Text -> IO Bool
checkDupeLink conf dlUrl
  = runLogger conf $ withPostgresqlPool (getConnStr conf) connections $ \pool -> liftIO $ do
      flip runSqlPersistMPool pool . asSqlBackendReader $ do
        runMigration migrateAll
        dupeLinks <- selectList [EntrySrcUrl ==. dlUrl] [Desc EntryUploadTime, LimitTo 1]
        return $ Prelude.null dupeLinks

-- |Returns the relevant DlTarget iff there is already a database entry for an image with matching hash
checkDupeHash :: Config -> Text -> IO (Maybe Text)
checkDupeHash conf md5
  = runLogger conf $ withPostgresqlPool (getConnStr conf) connections $ \pool -> liftIO $ do
      flip runSqlPersistMPool pool . asSqlBackendReader $ do
        runMigration migrateAll
        dupeHashes <- selectList [EntryMd5 ==. Just md5] [Desc EntryUploadTime, LimitTo 1]
        liftM (entryImgLoc . entityVal . Prelude.head) $ selectList [EntryImgLoc ==. Nothing] [Asc EntryUploadTime]

-- |Returns a list of download targets with a matching tag
getTaggedPosts :: Config -> Maybe Text -> IO [EntryId]
getTaggedPosts conf tag
  = runLogger conf $ withPostgresqlPool (getConnStr conf) connections $ \pool -> liftIO $ do
      flip runSqlPersistMPool pool . asSqlBackendReader $ do
        runMigration migrateAll
        case tag of
          Just t  -> liftM (Prelude.map $ tagMatchTaggedEntry . entityVal) $ selectList [TagMatchTag ==. t] [Asc TagMatchTaggedEntry]
          Nothing -> liftM (Prelude.map $ tagMatchTaggedEntry . entityVal) $ selectList [] [Asc TagMatchTaggedEntry]

-- |Returns an Entry given an ID
getEntryById :: Config -> EntryId -> IO (Maybe Entry)
getEntryById conf id
  = runLogger conf $ withPostgresqlPool (getConnStr conf) connections $ \pool -> liftIO $ do
      flip runSqlPersistMPool pool . asSqlBackendReader $ do
        runMigration migrateAll
        get id

getConnStr :: Config -> BS.ByteString
getConnStr conf
  = BS.pack ("host=" ++ dbAddr conf ++ " dbname=" ++ dbName conf ++ " user=" ++ dbName conf ++ " port=" ++ show (dbPort conf))

asSqlBackendReader :: ReaderT SqlBackend (NoLoggingT (ResourceT IO)) a -> ReaderT SqlBackend (NoLoggingT (ResourceT IO)) a
asSqlBackendReader = id
