-- |Module containing functions for downloading images located in source posts

{-# LANGUAGE OverloadedStrings   #-}

module Network.Erina.Downloader (downloaderThread) where

import Control.Lens
import Control.Concurrent.Chan
import Control.Monad
import Control.Monad.IO.StdErr
import Data.Maybe
import qualified Control.Exception as E
import qualified Data.Text as T
import qualified Data.ByteString.Lazy as B
import qualified Network.HTTP.Client as H
import qualified Network.HTTP.Types.Status as S
import Network.Wreq

import Network.Erina.Types.Target
import Network.Erina.Config
import Network.Erina.DataStore

imageExtensions = ["jpg", "jpeg", "png", "tif", "webm", "bmp", "gif"]

checkHttps :: T.Text -> Bool
checkHttps
  = ((==) "https") . head . (T.splitOn ":")

checkImageExtension :: T.Text -> Bool
checkImageExtension
  = (flip $ elem . (head . reverse . T.splitOn ".")) imageExtensions

-- |Continuously takes targets from the given channel and downloads the relevant image
downloaderThread ::  Config -> Int -> Chan DlTarget -> Chan (DlTarget, T.Text) -> IO ()
downloaderThread conf threadNo inChan doneChan
  = forever $ do
    sourcePost <- readChan inChan
    resLoc <- downloadTarget sourcePost conf `E.catch` flip netExceptHandle sourcePost
    if isJust resLoc then let Just t = resLoc in  writeChan doneChan (sourcePost, t) else return ()
      where
        netExceptHandle :: H.HttpException -> DlTarget -> IO (Maybe T.Text)
        netExceptHandle (H.HttpExceptionRequest _ c) p = netExceptHandle' c p
        netExceptHandle (e) p
          = putErrLn ("Encountered unexpected error: " ++ show e) >> return Nothing
        netExceptHandle' e@(H.StatusCodeException s _) p
          | S.statusCode (H.responseStatus s) == 404  = putErrLn (T.unpack ("404 not found at url " `T.append` dlUrl p)) >> return (Just "404")
          | S.statusCode (H.responseStatus s) == 403  = putErrLn (T.unpack ("403 forbidden at url " `T.append` dlUrl p)) >> return (Just "403")
        netExceptHandle' e p
          = putErrLn ("Encountered unexpected error: " ++ show e) >> return Nothing
      
downloadTarget :: DlTarget -> Config -> IO (Maybe T.Text)
downloadTarget tg conf
  = case checkImageExtension $ dlUrl tg of
      False -> specialTargetHandler tg conf >> return Nothing
      True  -> do
        alreadyDone <- case (md5 tg) of
          Just hash -> checkDupeHash conf $ T.pack hash
          Nothing   -> return $ Nothing
        case alreadyDone of 
          Just prev   -> return $ Just prev
          Nothing     -> do
            r <- get (T.unpack $ dlUrl tg)
            let img = r ^. responseBody
            let loc = destFileName tg conf
            B.writeFile (T.unpack loc) img 
            return $ Just loc
        

specialTargetHandler :: DlTarget -> Config -> IO (Maybe T.Text)
specialTargetHandler target conf
  = return Nothing

destFileName :: DlTarget -> Config -> T.Text
destFileName target conf
  = makeFilenameSafe conf filePath
    where
      origFileName = last $ T.splitOn "/"  $ dlUrl target
      fileName = "Source" `T.append` (T.pack $ show $ fromEnum $ dlSource target) `T.append` "_Post" `T.append` origFileName
      filePath = T.pack (storeDir conf) `T.append` fileName
        
makeFilenameSafe :: Config -> T.Text -> T.Text
makeFilenameSafe conf orig
  | T.isInfixOf "." orig  = flip T.append (T.append "." $ extension orig) $ T.take (maxFileLen conf - (extensionLen orig + 1)) nameBody
  | otherwise             = T.take (maxFileLen conf) orig
    where 
      nameBody = T.intercalate "." $ reverse . tail . reverse . T.splitOn "." $ orig
      extension = head . reverse . T.splitOn "."
      extensionLen = T.length . extension

