{-# LANGUAGE OverloadedStrings  #-}

module Network.Erina.DiscordPoster where

import Data.Random
import Data.Random.Source.IO
import Data.Random.Extras

import Data.Default
import Data.List
import Data.Maybe
import Control.Monad
import Control.Concurrent
import Control.Monad.IO.StdErr

import Network.HTTP.Req
import Network.HTTP.Client.MultipartFormData

import qualified Data.ByteString as BS
import qualified Data.Text as T
import qualified Data.Text.Encoding as TE

import Network.Erina.Config
import Network.Erina.DataStore

discordThread :: Config -> IO ()
discordThread conf
  = do
    case (discordDelay conf) of
      Nothing -> mempty
      Just d  -> forever $ do
        doPost conf
        threadDelay $ 1000000 * d

doPost :: Config -> IO ()
doPost conf
  = do
    let tagsLists   = discordPostTags conf
    let discordHook = discordWebHook conf
    case discordHook of
      Nothing -> mempty
      Just h  -> do
        res <- selectAndSendFile conf tagsLists $ T.pack h
        case res of
          Just err  -> putErrLn err
          Nothing   -> mempty

selectAndSendFile :: Config -> [[T.Text]] -> T.Text -> IO (Maybe String)
selectAndSendFile conf tags hookAddr
  = do
    viablePostLists <- if null tags then sequence [getTaggedPosts conf Nothing] else mapM (findTagged conf) tags
    let viablePosts = concat viablePostLists
    chosenPost <- choosePost conf viablePosts
    submissionError <- submitFile "" hookAddr $ T.unpack $ fromJust $ entryImgLoc chosenPost
    case submissionError of
      Just err  -> return $ Just err
      Nothing   -> return Nothing

findTagged :: Config -> [T.Text] -> IO [EntryId]
findTagged conf tags
  = do
    matchedTags <- mapM ((getTaggedPosts conf) . Just) tags
    return $ getCommon matchedTags
        
      
choosePost :: Config -> [EntryId] -> IO Entry
choosePost conf viablePosts
  = do
    imgChoice <- runRVar (choice viablePosts) StdRandom
    entry <- getEntryById conf imgChoice
    case entry of
      Nothing   -> choosePost conf viablePosts
      Just e    -> case (entryImgLoc e) of
        Nothing     -> choosePost conf viablePosts
        Just _      -> return e


-- |Gets common elements between any number of lists assuming they are all already sorted in ascending order
getCommon :: (Eq a) => [[a]] -> [a]
getCommon [] = []
getCommon xs = foldr1 intersect xs

submitFile :: T.Text -> T.Text -> FilePath -> IO (Maybe String)
submitFile accompanyingText hookUrl imgPath
  = do
    let image = partFile "file" imgPath
    let textContent = partBS "content" $ TE.encodeUtf8 accompanyingText
    let multipart = [image, textContent]
    case (parseUrlHttps (TE.encodeUtf8 hookUrl)) of
      Nothing             -> return $ Just "Failed to parse WebHook URI"
      Just (url, scheme)  -> do
        reqBody <- reqBodyMultipart multipart
        resp <- runReq def $ req POST url reqBody bsResponse mempty
        return $ case (responseStatusCode resp) of
          200 -> Nothing
          201 -> Nothing
          403 -> Just "Discord returned authorization error"
          404 -> Just "Discord returned page not found error"
          n   -> Just $ "Discord returned status code " ++ show n

