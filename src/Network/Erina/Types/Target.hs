-- |Module containing types for storing download targets
module Network.Erina.Types.Target where

import Data.Time.Clock
import Data.Text
import qualified Data.Text as T
import Database.Persist.Sql

-- |Enum for supported sources of content
data DlSource = FourChan | Pixiv | Danbooru | Gelbooru | Twitter
  deriving ( Show, Enum )
instance PersistField DlSource where
  toPersistValue = PersistInt64 . fromIntegral . fromEnum
  fromPersistValue (PersistInt64 i) = Right $ toEnum $ fromIntegral i 
  fromPersistValue x = Left $ T.pack $ "int64 Expected Integer, received: " ++ show x
instance PersistFieldSql DlSource where
  sqlType _ = SqlInt32

-- |Data type for storing data on downloads
data DlTarget = DlTarget 
                { dlSource :: DlSource
                , dlUrl :: Text
                , dlThumbUrl :: Maybe Text
                , dlId :: Text
                , dlCharacter :: Maybe Text
                , dlArtist :: Maybe Text
                , dlTags :: [Text]
                , md5 :: Maybe String
                , targetUploadTime :: UTCTime
                , targetLinkedText :: Text 
                } deriving ( Show )

