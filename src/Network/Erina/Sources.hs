-- |Module containing functions which run monitors for keeping sources up to date

module Network.Erina.Sources (runSourceMonitors) where

import Network.Erina.Config
import Network.Erina.Types.Target
import Network.Erina.DataStore

import Control.Concurrent
import Control.Concurrent.Chan
import Control.Monad
import Control.Monad.TakeWhileM
import Control.Concurrent
import Data.Time.Clock
import Data.Time.Calendar

-- |Runs source monitors for each of the given list of fetchAPIPage functions
runSourceMonitors :: [(Int -> String -> IO [DlTarget])] -> Config -> [String] -> Chan DlTarget -> Int -> IO [ThreadId]
runSourceMonitors monitors conf targetTags postChannel delay
  = mapM (\f -> forkIO (monitorTags f conf targetTags postChannel delay)) monitors

-- |Monitors a given set of tags using a given fetchAPIPage function, and pushes any additions into the given channel
monitorTags :: (Int -> String -> IO [DlTarget]) -> Config -> [String] -> Chan DlTarget -> Int -> IO ()
monitorTags fetchPostPage conf tags destPipe delay
  = forever $ do
    latest <- getLatestSourceLocal Danbooru conf
    newPosts <- bringTagsUpToDate fetchPostPage conf latest tags
    mapM_ (writeChan destPipe) $ reverse newPosts
    threadDelay $ delay * 1000000

bringTagsUpToDate :: (Int -> String -> IO [DlTarget]) -> Config -> UTCTime -> [String] -> IO [DlTarget]
bringTagsUpToDate fetchPostPage conf latest tags
  = do
    tagFeeds <- mapM (bringTagUpToDate fetchPostPage conf latest) tags
    let newPosts = concat tagFeeds
    return newPosts

bringTagUpToDate :: (Int -> String -> IO [DlTarget]) -> Config -> UTCTime -> String -> IO [DlTarget]
bringTagUpToDate fetchPostPage conf latest tag
  = do
    let resPages = map (flip fetchPostPage tag) [0..]
    res <- filterPostPages resPages latest
    putStrLn $ "Found " ++ show (length res) ++ " new matches for tag " ++ tag
    --Remove duplicate hits
    filterM (\x -> checkDupeLink conf $ dlUrl x) res
    

filterPostPages :: [IO [DlTarget]] -> UTCTime -> IO [DlTarget]
filterPostPages pages latestLocal
  = liftM (takeWhile (\x -> targetUploadTime x > latestLocal)) $ liftM concat $ takeWhileM (\x -> newestPostTime x > latestLocal) pages

newestPostTime :: [DlTarget] -> UTCTime
newestPostTime
  = foldl (\x y -> if x > targetUploadTime y then x else targetUploadTime y) $ UTCTime (ModifiedJulianDay 0) (secondsToDiffTime 0)

