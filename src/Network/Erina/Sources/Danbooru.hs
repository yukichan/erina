-- |API utilities for danbooru

{-# LANGUAGE DataKinds          #-}
{-# LANGUAGE OverloadedStrings  #-}
{-# LANGUAGE DeriveGeneric      #-}
{-# LANGUAGE TypeOperators      #-}

module Network.Erina.Sources.Danbooru (fetchDanbooruPostPage) where

import Prelude hiding ( id )

import Control.Monad
import Data.Aeson
import Data.Int
import Data.Maybe
import Data.List.Split
import Data.Proxy
import Data.Time.Format
import Data.Time.Clock
import Data.Time.Calendar
import Data.Text (pack, Text(..))
import GHC.Generics
import Network.HTTP.Client (newManager, defaultManagerSettings)
import Network.HTTP.Client.TLS
import Servant.API
import Servant.Client

import Network.Erina.Config
import Network.Erina.Types.Target

-- |Data type for storing all data from danbooru API
data DbPost = DbPost
              { id :: Int64
              , source :: String
              , md5 :: Maybe String
              , created_at :: String
              , image_width :: Int
              , image_height :: Int
              , tag_string :: String
              , pixiv_id :: Maybe Int64
              , tag_string_artist :: String
              , tag_string_character :: String
              , tag_string_copyright :: String
              , tag_string_general :: String
              , file_url :: Maybe String
              , large_file_url :: Maybe String
              , preview_file_url :: Maybe String
              } deriving (Show, Generic)
instance FromJSON DbPost

data DbNote = DbNote
              { nid :: Int64
              , body :: String
              } deriving (Show, Generic)
instance FromJSON DbNote

type DbPostAPI = "posts.json" :> QueryParam "limit" Int :> QueryParam "page" Int :> QueryParam "tags" String :> Get '[JSON] [DbPost]

dbBaseUrl :: String
dbBaseUrl = "http://danbooru.donmai.us"

dbPostAPI :: Proxy DbPostAPI
dbPostAPI = Proxy

posts :: Maybe Int -> Maybe Int -> Maybe String -> ClientM [DbPost]
posts = client dbPostAPI

-- |Fetches a single page of results from the API given the page number and tag search term
fetchDanbooruPostPage :: Int -> String -> IO [DlTarget]
fetchDanbooruPostPage pg tags
  = do
    putStrLn ("Fetching Danbooru page " ++ show pg ++ " for tag " ++ tags)
    manager <- newManager tlsManagerSettings
    res <- runClientM (posts (Nothing) (Just (pg + 1)) (Just tags)) (ClientEnv manager $ BaseUrl Https "danbooru.donmai.us" 443 "")
    case res of
      Left err    -> (>>) (putStrLn $ "Error: " ++ show err) $ return []
      Right ps    -> return $ map toStandardTarget ps

-- |Converts a dabooru target data structure to a standard DlTarget
toStandardTarget :: DbPost -> DlTarget
toStandardTarget post
  = DlTarget Danbooru dlUrl dlThumb dlId character artist dlTags dlmd5 ulTime linkedText
    where
      dlUrl = pack $ (if isJust $ file_url post then ((++) dbBaseUrl) . fromJust . file_url else source) post
      dlThumb = fmap (\f -> pack $ dbBaseUrl ++ f) $ preview_file_url post
      dlId = pack $ show $ id post
      character = Just $ pack $ tag_string_character post
      artist = Just $ pack $ tag_string_artist post
      dlTags = map pack $ splitOn " " $ tag_string post
      ulTime = parseTimeOrError True defaultTimeLocale (iso8601DateFormat $ Just "%H:%M:%S%Q%z") $ created_at post
      dlmd5 = Network.Erina.Sources.Danbooru.md5 post
      linkedText = "" -- TODO: Actually add linked text
