-- |API utilities for danbooru

{-# LANGUAGE DataKinds          #-}
{-# LANGUAGE OverloadedStrings  #-}
{-# LANGUAGE DeriveGeneric      #-}
{-# LANGUAGE TypeOperators      #-}

module Network.Erina.Sources.Gelbooru (fetchGelbooruPostPage) where

import Prelude hiding ( id )

import Control.Monad
import Data.Aeson
import Data.Int
import Data.Maybe
import Data.List.Split
import Data.Proxy
import Data.Time.Clock
import Data.Time.Clock.POSIX
import Data.Text (pack, Text(..))
import GHC.Generics
import Network.HTTP.Client (newManager, defaultManagerSettings)
import Network.HTTP.Client.TLS
import Servant.API
import Servant.Client

import Network.Erina.Config
import Network.Erina.Types.Target

-- |Data type for storing all data from gelbooru API
data GbPost = GbPost
              { id :: Int64
              , source :: String
              , hash :: Maybe String
              , change :: Integer
              , width :: Int
              , height :: Int
              , tags :: String
              , file_url :: Maybe String
              } deriving (Show, Generic)
instance FromJSON GbPost


type DbPostAPI = "index.php" 
                    :> QueryParam "page" String
                    :> QueryParam "s" String
                    :> QueryParam "q" String
                    :> QueryParam "json" Int 
                    :> QueryParam "limit" Int 
                    :> QueryParam "pid" Int 
                    :> QueryParam "tags" String 
                    :> Get '[JSON] [GbPost]

baseUrl :: String
baseUrl = "gelbooru.com"

gbPostAPI :: Proxy DbPostAPI
gbPostAPI = Proxy

posts :: Maybe String -> Maybe String -> Maybe String -> Maybe Int -> Maybe Int -> Maybe Int -> Maybe String -> ClientM [GbPost]
posts = client gbPostAPI

-- |Fetches a single page of results from the API given the page number and tag search term
fetchGelbooruPostPage :: Int -> String -> IO [DlTarget]
fetchGelbooruPostPage pg tags
  = do
    putStrLn ("Fetching Gelbooru page " ++ show pg ++ " for tag " ++ tags)
    manager <- newManager tlsManagerSettings
    res <- runClientM (posts  (Just "dapi") 
                              (Just "post") 
                              (Just "index") 
                              (Just 1) 
                              Nothing 
                              (Just pg) 
                              (Just tags)) (ClientEnv manager $ BaseUrl Https baseUrl 443 "")
    case res of
      Left err    -> (>>) (putStrLn $ "Error: " ++ show err) $ return []
      Right ps    -> return $ map toStandardTarget ps

-- |Converts a gelbooru target data structure to a standard DlTarget
toStandardTarget :: GbPost -> DlTarget
toStandardTarget post
  = DlTarget Gelbooru dlUrl Nothing dlId Nothing Nothing dlTags dlmd5 ulTime linkedText
    where
      dlUrl = pack $ (if isJust $ file_url post then fromJust . file_url else source) post
      dlId = pack $ show $ id post
      dlTags = map pack $ splitOn " " $ Network.Erina.Sources.Gelbooru.tags post
      ulTime = posixSecondsToUTCTime $ fromIntegral (change post)
      dlmd5 = Network.Erina.Sources.Gelbooru.hash post
      linkedText = "" -- TODO: Actually add linked text
