-- |Module handling launching of all source monitors as well as download threads

{-# LANGUAGE OverloadedStrings   #-}

module Network.Erina.Monitor(monitorSources) where

import qualified Data.ByteString as B
import qualified Data.Text as T
import Data.List.Split
import Data.Traversable
import Control.Concurrent
import Control.Concurrent.MVar
import Control.Concurrent.Chan
import Control.Monad
import Network.HTTP
import Network.URI (parseURI)

import Network.Erina.DataStore
import Network.Erina.DiscordPoster
import Network.Erina.Downloader
import Network.Erina.Sources
import Network.Erina.Sources.Danbooru
import Network.Erina.Sources.Gelbooru
import Network.Erina.Config
import Network.Erina.Types.Target

sourceFetchers = [fetchDanbooruPostPage, fetchGelbooruPostPage]

-- |Creates channels and launches all monitor and download threads after resuming any incomplete downloads
monitorSources :: Config -> IO ()
monitorSources conf
  = do
    -- Setup channels for thread communication
    postChannel <- newChan :: IO (Chan DlTarget)
    downloadQueue <- newChan :: IO (Chan DlTarget)
    recordChannel <- newChan :: IO (Chan (DlTarget, T.Text))
    -- Attempt retry of existing images
    nullDownloads <- getNullDownloads conf
    mapM_ (writeChan downloadQueue) nullDownloads
    print "Cleared incomplete downloads. Now monitoring sources..."
    -- Load config
    let targetTags = tags conf
    let delay = updateDelay conf
    -- Launch source monitors
    monitors <- runSourceMonitors sourceFetchers conf targetTags postChannel delay
    -- Launch Processor thread
    processor <- forkIO $ processPosts conf postChannel downloadQueue
    -- Launch Downloader thread
    downloader <- forkIO $ downloaderThread conf 0 downloadQueue recordChannel
    -- Launch Discord posting thread
    poster <- forkIO $ discordThread conf
    updateFiles conf recordChannel

processPosts :: Config -> Chan DlTarget -> Chan DlTarget -> IO ()
processPosts conf inputChan outputChan
  = forever $ do
    sourcePost <- readChan inputChan
    insertTarget sourcePost conf
    writeChan outputChan sourcePost

makeFilenameSafe :: Config -> T.Text -> T.Text
makeFilenameSafe conf orig
  = T.intercalate "." $ take (maxFileLen conf - extensionLen orig) $ reverse . tail . reverse . T.splitOn "." $ orig
    where 
      extensionLen = T.length . head . reverse . T.splitOn "."
          
updateFiles :: Config -> Chan (DlTarget, T.Text) -> IO ()
updateFiles conf inputChan
  = forever $ do
    (post, file) <- readChan inputChan
    assignFile post conf file

