-- |Main entry point into program
module Network.Erina where

import Network.Erina.Config
import Network.Erina.Monitor

-- |Loads config and starts monitor
run :: IO ()
run 
  = do
    conf <- getConf
    monitorSources conf
